A website designed for post-production audio jobs.

The project stems from a business plan created in 75-391 (New Venture Formation) with 2 other classmates. Took it one step further and started developing a prototype website. Even though it is incomplete, it is my first take at building a website and contains a look into how I learn/code. It contains a hack implementation and an object-oriented implementation.

**Core Features:**

* user registration, login, and logout

* creation and listings of job posts

* message inbox between users

* edit profile with uploading of picture and audio files

* messages passed between pages

* autoload classes

* setup page for first time run

**Folder Structure:**

* ./basics_imp - contains the first hack implementation of the website. It contains the most features

* ./oop_imp - contains the object-oriented implementation

* ./laravel_imp - contains some laravel code. It was abandoned while I was learning laravel.